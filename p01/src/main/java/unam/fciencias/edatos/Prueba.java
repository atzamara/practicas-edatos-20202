package unam.fciencias.edatos;

import java.util.Arrays;
import java.util.Random;
import java.util.spi.LocaleNameProvider;

public class Prueba {
    private int[] arreglo;
    private int longiArreglo;




    public Prueba(int longiArreglo){

        this.longiArreglo = longiArreglo;
        arreglo = new int[longiArreglo];

        for (int i = 0; i < arreglo.length; i++) {
            arreglo[i] = i+1;
        }
    }

    public long promedioBusqueda(int repit){
        Random random = new Random();
        long promedio = 0;
        for (int i = 0; i < repit; i++) {
            long comienzo = System.nanoTime();
            busquedaBinaria(random.nextInt(longiArreglo + 1 ));
            long finall = System.nanoTime();
            promedio += finall - comienzo;
        }
        return promedio/repit;
    }




    private int busquedaBinaria(int dato) {
        int inicio = 0;
        int fin = arreglo.length - 1;
        int posicion;
        while (inicio <= fin) {
            posicion = (inicio + fin) / 2;
            if (arreglo[posicion] == dato)
                return posicion;
            else if (arreglo[posicion] < dato) {
                inicio = posicion + 1;
            } else {
                fin = posicion - 1;
            }
        }
        return -1;
    }

    @Override
    public String toString() {
        return "("+ longiArreglo + ", " + promedioBusqueda(10) + ")";
    }

}