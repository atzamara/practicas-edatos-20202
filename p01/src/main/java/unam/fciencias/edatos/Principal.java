package unam.fciencias.edatos;

import java.util.ArrayList;
import java.util.Collection;

public class Principal {

    public static void main(String[] args) {

        Prueba ejemplar2 = new Prueba(2);
        Prueba ejemplar5 = new Prueba(5);
        Prueba ejemplar10 = new Prueba(10);
        Prueba ejemplar100 = new Prueba(100);
        Prueba ejemplar1000 = new Prueba(1000);
        Prueba ejemplar2000 = new Prueba(2000);

        ejemplar2.promedioBusqueda(10);
        ejemplar5.promedioBusqueda(10);
        ejemplar10.promedioBusqueda(10);
        ejemplar100.promedioBusqueda(10);
        ejemplar1000.promedioBusqueda(10);
        ejemplar2000.promedioBusqueda(10);


        Collection<Prueba> coleccion = new ArrayList<>();
        coleccion.add(ejemplar2);
        coleccion.add(ejemplar5);
        coleccion.add(ejemplar10);
        coleccion.add(ejemplar100);
        coleccion.add(ejemplar1000);
        coleccion.add(ejemplar2000);
        System.out.println(coleccion);
    }
}
